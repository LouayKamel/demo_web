// We need to import the CSS so that webpack will load it.
// The MiniCssExtractPlugin is used to separate it out into
// its own CSS file.
import css from "../css/app.css"

// webpack automatically bundles all modules in your
// entry points. Those entry points can be configured
// in "webpack.config.js".
//
// Import dependencies
//
import "phoenix_html"


import 'sigma/build/plugins/sigma.renderers.edgeLabels.min.js';
import 'sigma/build/plugins/sigma.renderers.customShapes.min.js';


import 'sigma/src/renderers/sigma.renderers.webgl.js';
import 'sigma/src/renderers/webgl/sigma.webgl.edges.def.js';

import 'sigma/src/renderers/webgl/sigma.webgl.nodes.def.js';

import 'sigma/src/renderers/webgl/sigma.webgl.edges.arrow.js';

import 'sigma/plugins/sigma.layout.forceAtlas2/worker.js';
import 'sigma/plugins/sigma.layout.forceAtlas2/supervisor.js';
import 'sigma/plugins/sigma.plugins.animate/sigma.plugins.animate.js';
import 'dagre-webpack/dist/dagre.js';
import 'sigma/plugins/sigma.layout.dagre/sigma.layout.dagre.js';
import 'sigma/plugins/sigma.layout.dagre/sigma.layout.dagre.js';




// Import local files
//
// Local files can be imported directly using relative paths, for example:
// import socket from "./socket"

import socket from "./socket"



var s = new sigma({
    renderer: {
        container: 'graph-container',
        type: 'canvas'
          },
    settings: {
      minArrowSize: 8
    }
});


s.startForceAtlas2();

let channel = socket.channel('iota:tracing', {});
channel.on('phx_reply', (data) => {
  let response = data.response
  if (Object.keys(response).length) {
    if (response.query == 'both') {
      s.graph.addNode({
        id: response.node.id,
        type: response.node.type,
        x: Math.random(),
        y: Math.random(),
        size: 0.2,
        color: "grey"
      });
      s.graph.addEdge({
        id: response.edge.id,
        source: response.edge.source,
        target: response.edge.target,
        type: 'arrow',
        label: response.edge.label
      });
    } else if (response.query == 'edge') {
      s.graph.addEdge({
        id: response.edge.id,
        source: response.edge.source,
        target: response.edge.target,
        type: 'arrow',
        label: response.edge.label
      });
    };
    };
    
  });
channel.join()
  .receive("ok", resp => { console.log("Joind IOTA Tracing Channel", resp)})


let address = document.getElementById('address');

address.addEventListener('keypress', function(event) {
  if (event.keyCode == 13 && address.value.length == 81) {
    s.graph.addNode({
      id: address.value,
      type: 'circle',
      x: 0,
      y: 0,
      size: 0,
      color: '#ff0'
    });
    channel.push('all', {
      address: address.value
    });
    console.log("Address in Progress:", address.value);
  //  inprogress.innerHTML = address.value;
  //  address.value = '';
    address.disabled = true;
  }
});

document.getElementById("all").addEventListener("click", all);
document.getElementById("from").addEventListener("click", from);
document.getElementById("to").addEventListener("click", to);

function all() {
  if (address.value.length == 81) {
    s.graph.addNode({
      id: address.value,
      type: 'circle',
      x: 0,
      y: 0,
      size: 1,
      color: '#ff0'
    });
    channel.push('all', {
      address: address.value,
    });
    console.log("Address in Progress:", address.value);
    address.disabled = true;
    window.setTimeout(function() {s.killForceAtlas2()}, 5000);
    window.setTimeout(function() {s.startForceAtlas2()}, 10000);
    window.setTimeout(function() {s.killForceAtlas2()}, 19000);
  }
}
function from() {
  if (address.value.length == 81) {
    s.graph.addNode({
      id: address.value,
      type: 'circle',
      x: 0,
      y: 0,
      size: 1,
      color: '#ff0'
    });
    channel.push('from', {
      address: address.value,
    });
    console.log("Address in Progress:", address.value);
    address.disabled = true;
    window.setTimeout(function() {s.killForceAtlas2()}, 5000);
    window.setTimeout(function() {s.startForceAtlas2()}, 10000);
    window.setTimeout(function() {s.killForceAtlas2()}, 19000);
  }
}
function to() {
  if (address.value.length == 81) {
    s.graph.addNode({
      id: address.value,
      type: 'circle',
      x: 0,
      y: 0,
      size: 1,
      color: '#ff0'
    });
    channel.push('to', {
      address: address.value,
    });
    console.log("Address in Progress:", address.value);
    address.disabled = true;
    window.setTimeout(function() {s.killForceAtlas2()}, 5000);
    window.setTimeout(function() {s.startForceAtlas2()}, 10000);
    window.setTimeout(function() {s.killForceAtlas2()}, 190000);
  }
}
s.refresh();

//window.setTimeout(function() {s.killForceAtlas2()}, 11000);

window.setTimeout(function() {s.refresh()}, 10000);



// Instantiate sigma:
