# Since configuration is shared in umbrella projects, this file
# should only configure the :demo_web application itself
# and only for organization purposes. All other config goes to
# the umbrella root.
use Mix.Config

# General application configuration
config :demo_web,
  generators: [context_app: :demo]

# Configures the endpoint
config :demo_web, DemoWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "GffUAf/TRqUhZ2wQ3OE1MPTqIZhEHJiFvV9pACdRccogjCSXgC5ohpS2JyB3o6Gu",
  render_errors: [view: DemoWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: DemoWeb.PubSub, adapter: Phoenix.PubSub.PG2]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
