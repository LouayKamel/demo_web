defmodule DemoWeb.IotaChannel do

  use DemoWeb, :channel

  def join("iota:tracing", payload, socket) do
    if authorized?(payload) do
      send(self(), :after_join)
      {:ok, socket}
    else
      {:error, %{reason: "unauthorized"}}
    end
  end


  # reply to everyone in the current topic (iota:tracing).
  def handle_in("all", payload, socket) do
    address = Map.get(payload, "address")
    trace(:all, address, socket)
    {:noreply, socket}
  end

  def handle_in("from", payload, socket) do
    address = Map.get(payload, "address")
    trace(:from, address, socket)
    {:noreply, socket}
  end

  def handle_in("to", payload, socket) do
    address = Map.get(payload, "address")
    trace(:to, address, socket)
    {:noreply, socket}
  end

  # sending replies to requests from the client
  def handle_in("ping", payload, socket) do
    {:reply, {:ok, payload}, socket}
  end


  def trace(call, address, socket) do
    {pid, j, t, c, b} = ref = socket_ref(socket)
    ref_id = :rand.uniform(99999999)
    pid_list = :erlang.pid_to_list(pid)
    FastGlobal.put(ref_id, {pid_list, j, t, c, b})
    # trace logic
    producer = Demo.Engine.Ring.get_producer(address)
    send(producer, {call, address, ref_id, nil})
  end















  def handle_info(:after_join, socket) do

    # push to the socket if needed

    # :noreply
    {:noreply, socket}
  end





























  def reply_async(ref_id, reply_map) do
    {pid_list, j, t, c, b} = FastGlobal.get(ref_id)
    pid = :erlang.list_to_pid(pid_list)
    reply({pid, j, t, c, b}, {:ok, reply_map})
  end




  defp authorized?(_payload) do
    true
  end


end
